
let canvas= document.getElementById('canvas');
let ctx=canvas.getContext('2d');


function sliderChanged(){
    let length = document.getElementById("waveLength").value;
    let height = document.getElementById("waveHeight").value;
    let amount = document.getElementById("waveAmount").value;
 
    ctx.clearRect(0,0,canvas.width, canvas.height);
    drawWave(length,height,amount);

}
function drawWave(length, height, amount){
    let y=0;
    ctx.moveTo(0,250);
    ctx.beginPath();
    for (let x=0; x<length; x++)
    {        
        y=Math.cos(x/amount)*height+250;
        ctx.lineTo(x,y);
    }
    ctx.stroke();
}