

    let canvas= document.getElementById('canvas');
    let ctx=canvas.getContext('2d');




class Point{
    constructor(x,y){
        this.x=x;
        this.y=y;
    }
}

function blackTriangle(p1,p2,p3){
    ctx.clearRect(0,0,canvas.width, canvas.height);
    ctx.beginPath();
    ctx.moveTo(p1.x,p1.y);
    ctx.lineTo(p2.x, p2.y);
    ctx.lineTo(p3.x, p3.y);
    ctx.fillStyle="black";
    ctx.fill();
    ctx.closePath();
}


function onLoad(){

    console.log("hi");

    /*
    p1= new Point(500, 50);
    p2= new Point(0, 750);
    p3= new Point(1000, 750);

    drawTriangles(p1, p2, p3,5);
    */

}

function sliderChanged(){
    let size = document.getElementById("size").value;
    let amount = document.getElementById("amount").value;

        let tri, height;
        

        if(amount*size<=1000)
        {
            console.log("calc");
            tri=amount*size;
            height=Math.sqrt(3)*tri/2;
            p1= new Point(tri/2, 0);
            p2= new Point(0, height);
            p3= new Point(tri, height);

            blackTriangle(p1,p2,p3);
            drawTriangles(p1, p2, p3, amount);
        }
 
}


function drawTriangles(pTop, pLeft, pRight, n){
    let middleRight=middleLine(pRight, pTop);
    let middleLeft=middleLine(pLeft, pTop);
    let middleButtom=middleLine(pLeft, pRight);

    drawOneUnit(pTop, pLeft, pRight);
    if(n==1)
    {
        blackTriangle();
        return;
    }

    if(n==2)
    {
        drawOneUnit(pTop, pLeft, pRight);
        return;
    }

        drawTriangles(middleRight, middleButtom ,pRight, n-1);//RIGHT
        console.log("RIGHT "+n);
        drawTriangles(middleLeft, pLeft, middleButtom, n-1);//LEFT
        console.log("LEFT "+n);
        drawTriangles(pTop, middleLeft, middleRight, n-1);//TOP  
        console.log("TOP "+n);
    

}


function drawOneTriangle(p1, p2, p3){
    ctx.beginPath();
    ctx.moveTo(p1.x, p1.y);
    ctx.lineTo(p2.x, p2.y);
    ctx.lineTo(p3.x, p3.y);
    ctx.closePath();
    ctx.fillStyle="white";
    ctx.fill();
}

function drawOneUnit(pTop, pLeft, pRight){
    drawOneTriangle(middleLine(pRight,pLeft),middleLine(pTop,pLeft),middleLine(pTop,pRight));
}



function middleLine(p1,p2)
{
    x=(p1.x+p2.x)/2;
    y=(p1.y+p2.y)/2;

    return new Point(x,y);    
}