function time(){
    let d = new Date();
    document.getElementById("mainDate").innerHTML = d.toLocaleString();
}


function start(){
    let i=0;
    setInterval(function(){
        if(i==10)
            return true;
        i++;
        time();
},1000);
}


function startProm(){  
    let p  = new Promise(function(resolve, reject) {
    if(!start())
        resolve("hi");           
    });

    p.then(function(value){
        console.log(value);
    })
}