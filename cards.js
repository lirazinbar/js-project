const minLettersInName = 2;
const regexEmail=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class Person {
    constructor(name, profession, email, index) {
        this.name = name;
        this.profession = profession;
        this.email = email;
        this.index= index;
    }
}

let people = [];
let name;
let profession;
let email;

let newLi;
let mainCar;
let deleteButton;
let globalIndex=0;


let myStorage = window.localStorage;

/*
let text = '{ "cards" : [' +
    '{ "name":"" , "profession":"", "email":"" }]}';
let obj = JSON.parse(text);
let text=JSON.stringify(obj);
*/



/*
for (let i=0; cards.length; i++){
    newLi.innerHTML=cards[i].name+" "+cards[i].profession+" "+cards[i].email;
    mainCard.appendChild(newLi);
}*/

function loadFromLocalStorage() {
    people = JSON.parse(myStorage.getItem('people') || `[]`);
    globalIndex = JSON.parse(myStorage.getItem('globalIndex'));
}

function saveToLocalStorage() {
    myStorage.setItem('people', JSON.stringify(people));
    myStorage.setItem('globalIndex', JSON.stringify(globalIndex));
}

function onLoad() {
    loadFromLocalStorage();
    showCards();
}

function showCards() {
    people.forEach(person => addCard(person));
}

function addPerson(name, profession, email) {
    const person = new Person(name, profession, email, globalIndex);
    globalIndex++;
    people.push(person);
    addCard(person);
    saveToLocalStorage();
}

function extractPersonFromForm() {
    name = document.getElementById("name").value;
    profession = document.getElementById("profession").value;
    email = document.getElementById("email").value;
    profession = profession.toString().replace("פקיד", "");


    if (validUser(name, email))
        addPerson(name, profession, email);

    /*
    if (name.toString().length >= minLettersInName) {
        addPerson(name, profession, email);
    }
    */
}

function validUser(name, email){
    if (name.toString().length < minLettersInName) 
    {
        console.log("invalid name ");
        return false;
    }
    
    if (!regexEmail.test(email.toString()))
    {
        console.log("invalid email ");
        return false;
    }
    return true;
}

function addCard(person) {
    newDiv = document.createElement("div");
    main=document.getElementById("main");
    newDiv.classList.add("card");
    
    

    card1 = document.createElement("div");
    card1.classList.add("font");
    card2 = document.createElement("div");
    card2.classList.add("font");
    card3 = document.createElement("div");
    card3.classList.add("font");
    card1.innerHTML = person.name;
    card2.innerHTML = person.profession;
    card3.innerHTML = person.email;


    newDiv.appendChild(card1);
    newDiv.appendChild(card2);
    newDiv.appendChild(card3);
    main.appendChild(newDiv);

    //newLi = document.createElement("li");
    //mainCard = document.getElementById("main-cards");
    //newLi.innerHTML = `${person.name} ${person.profession} ${person.email}`;
    //mainCard.appendChild(newLi);
    deleteButton = document.createElement("button");
    deleteButton.innerHTML = "remove";
    deleteButton.id=person.index;
    console.log("the id is  "+deleteButton.id);
    deleteButton.setAttribute("onclick", "removeCard(this)");
    //newLi.appendChild(deleteButton);
    newDiv.appendChild(deleteButton);

}

function removeCard(button){    
    people.find(function(person, index){
        if(button.id== person.index){
            console.log("...");
            people.splice(index, 1);
            button.parentElement.remove();
            saveToLocalStorage();
            return true;
        }
        saveToLocalStorage();
        return false;
    })

}


